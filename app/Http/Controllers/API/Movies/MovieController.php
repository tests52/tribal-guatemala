<?php

namespace App\Http\Controllers\API\Movies;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use PhpParser\Comment;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends Controller
{

    /**
     * @OA\Get(
     *   path="/api/v1/movies",
     *   tags={"movies"},
     *   summary="Get list of movies",
     *   description="Returns list of movies",
     *   @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *   ),
     *
     *   @OA\Parameter(
     *      name="page",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *   ),
     *)
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiKey = env('API_KEY_TMDB' , 'd233eb0a2c528f1943e3054380e426c2');

        $page = $request->page != null ? $request->page : 1;

        $language = 'es-ES';
        $response = Http::get('https://api.themoviedb.org/3/movie/now_playing?api_key=' . $apiKey . '&page=' . $page. '&language=' . $language);

        if($response->status() == 200) {
            return response()->json(['success' => json_decode($response->body())])->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return response()->json(['success' => json_decode($response->body())])->setStatusCode(Response::HTTP_UNAUTHORIZED);
        }
    }
    /**
     * @OA\Get(
     *   path="/api/v1/movies/{id}",
     *   tags={"movies"},
     *   summary="Get specific movie by Id",
     *   description="Returns specific movie by Id",
     *   @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *   ),
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *   ),
     *)
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apiKey = env('API_KEY_TMDB' , 'd233eb0a2c528f1943e3054380e426c2');
        $language = 'es-ES';
        $response = Http::get('https://api.themoviedb.org/3/movie/' . intval($id) . '?api_key=' . $apiKey . '&language=' . $language);

        if($response->status() == 200) {
            return response()->json(['success' => json_decode($response->body())])->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return response()->json(['success' => json_decode($response->body())])->setStatusCode(Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @OA\Post(
     *   path="/api/v1/movies/comment/{id}",
     *   tags={"movies"},
     *   summary="Post comment and rating to movie by Id",
     *   description="Returns specific movie by Id",
     *   @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *   ),
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *
     *   @OA\Parameter(
     *      name="rating",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *
     *   @OA\Parameter(
     *      name="comment",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request",
     *   ),
     *)
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment($id, CommentRequest $commentRequest)
    {
        $comment = [
            "user_id" => auth()->id(),
            "api_movie_id" => intval($id),
            "rating" => $commentRequest->rating,
            "comment" => $commentRequest->comment,
        ];

        $response = Comments::create($comment);

        if($response) {
            $success['comment'] =  $comment;
            $success['user'] =  auth()->user();
            $success['status'] = true;
             return response()->json(['success' => $success])->setStatusCode(Response::HTTP_CREATED);
        } else {
            return response()->json(['error' => 'Bad Request'], Response::HTTP_BAD_REQUEST);
        }
    }



    /**
     * @OA\Get(
     *   path="/api/v1/movies/comment/{id}",
     *   tags={"movies"},
     *   summary="Get collection comments of movie by Id",
     *   description="Returns collection comments of movie by Id",
     *   @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *   ),
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request",
     *   ),
     *)
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getComment($id)
    {
        $commentId =  intval($id);
        return CommentResource::collection(Comments::all()->where('api_movie_id' , $commentId)->forPage(0, 10));
    }


}

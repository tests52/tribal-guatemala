<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Laravel Tribal Guatemala Documentation",
     *      description="L5 Swagger Documentacion API con OAuth2 , Tomar en cuenta que en la raiz del proyecto esta el documento postman para realizar pruebas. Sin embargo tambien se deja todo documentado con Swagger , puede encontrar documentacion de The Movie DB en https://developers.themoviedb.org/3/movies/get-now-playing",
     *      @OA\Contact(
     *          email="codemax120@gmail.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      ),
     *      @OA\License(
     *          name="The Movie DB",
     *          url="https://developers.themoviedb.org/3/movies/get-now-playing"
     *      ),
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Local API Server"
     * )
     *
     * @OA\SecurityScheme(
     *     securityScheme="bearerAuth",
     *     type="http",
     *     scheme="bearer",
     * )
     *
     * @OA\Tag(
     *     name="Projects",
     *     description="API Endpoints of Projects"
     * )
     */
}
